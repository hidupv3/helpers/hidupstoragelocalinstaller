#!/bin/bash
# ---------------------------------------------------------------------
# Hidup Storage Installer
#
# Copyright © 2022  Hidup Consulting
#
# All rights reserved
# ----------------------------------------------------------------------

export  VERSION="0.1.0"
export  HSLI_DIR=/opt/HidupStorageLocaInstaller/
export  LOG=/tmp/HidupStorageLocaInstaller.$(date "+%Y%m%d.%H%M%S").log
export  ERROR_LOG=/tmp/HidupStorageLocaInstaller.$(date "+%Y%m%d.%H%M%S").error

export  RED="\e[31m\e[1m" \
        YELLOW="\e[33m\e[1m" \
        BLUE="\e[94m\e[1m" \
        GREEN="\e[92m\e[1m" \
        STOP="\e[0m"

function banner() {
  echo
  echo -e "${BLUE}# ---------------------------------------------------------------------${STOP}"
  echo -e "${BLUE}#${YELLOW} Hidup Storage Installer ${VERSION}${STOP}"
  echo -e "${BLUE}# ---------------------------------------------------------------------${STOP}"
  echo
}

function message() {
    textcolor=$1; shift
    restest=$1; shift

    echo -e "${textcolor}[$restest]${STOP} $*"
}

function clear_and_exit() {
  rm $ERROR_LOG 2> /dev/null
  echo -e "${STOP}"
  exit ${1-0}
} 

function run() {
    text=$1; shift
    $* > $ERROR_LOG 2>> $ERROR_LOG
    if [[ $? != 0 ]]
    then 
      message ${RED} "Error" "$text - Please read logfile $LOG"
      echo -e "${RED}"
      cat $ERROR_LOG | tee -a $LOG 
      clear_and_exit 1
    else
      message "${GREEN}" "OK" "$text"
      cat $ERROR_LOG >> $LOG
    fi
}  

function check_pre() {
    # Check root user
    if [[ $(id -u) != "0" ]]
    then
      echo "I need root privileges to install."
      return 1
    fi
    # Checking operating system
    source /etc/os-release
    case "$ID-$VERSION_ID" in
      "rocky-8.5")
        echo "Found Rokcy Linux 8.5 - All rigth"
        ;;
      *)
        echo "Sorry, your operating system is not supported jet. Ask us to support@hidup.io"
        return 1
        ;;
    esac
}

function installing_docker() {
  # Only RHEL family
  run "Updating OS" dnf update -y
  run "Installing yum-utils" dnf install -y dnf-utils
  run "Installing docker repository" yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
  run "Installing docker" dnf install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
  run "Enabling docker" systemctl enable --now docker
  run "Starting docker" docker run --rm hello-world
  [[ ! -d "$HSLI_DIR" ]] && mkdir -p "$HSLI_DIR"
}

function installing_software() {
  run "Installing software base" curl -s -o ${HSLI_DIR}/docker-compose.yml https://gitlab.com/hidupv3/helpers/hidupstoragelocalinstaller/-/raw/main/docker-compose.yml
  cd $HSLI_DIR
  run "Starting Hidup Storage Agent" "docker compose up -d"
  cd - > /dev/null
}

banner
run "Checking prerequisites" check_pre
installing_docker
installing_software
clear_and_exit
